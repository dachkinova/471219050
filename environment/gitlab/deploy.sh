#!/bin/bash
set -e
set -x;

export PROJECT_ID=$(gcloud config get-value core/project)

########################################################################################################################
export UNIQUE_IDENTIFIER="050"
export NAMESPACE="gitlab-${UNIQUE_IDENTIFIER}"
########################################################################################################################

kubectl create namespace ${NAMESPACE} || true
kubectl apply -f k8s --namespace ${NAMESPACE}

